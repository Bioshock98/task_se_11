package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove project and related tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT'S ID");
        printProjects(projectEndpoint.findAllProjectsByUserId(bootstrap.getSession()));
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projectEndpoint.getProjectIdByNumber(bootstrap.getSession(), projectNumber);
        taskEndpoint.removeTasksByProjectId(bootstrap.getSession(), projectId);
        projectEndpoint.removeProjectByUserId(bootstrap.getSession(), projectId);
        System.out.println("[PROJECT REMOVED]");
    }
}

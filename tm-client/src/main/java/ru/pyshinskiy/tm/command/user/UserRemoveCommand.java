package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUsers;

public final class UserRemoveCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSession() == null) return false;
        return bootstrap.getSession().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "user_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove existing user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER ID");
        printUsers(bootstrap.getUserEndpoint().findAllUsers(bootstrap.getSession()));
        @NotNull final IUserEndpoint userEndpoint = bootstrap.getUserEndpoint();
        final int userNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String userId = userEndpoint.getUserIdByNumber(bootstrap.getSession(), userNumber);
        bootstrap.getUserEndpoint().removeUser(bootstrap.getSession(), userId);
        System.out.println("USER REMOVED");
    }
}

package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Task;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = bootstrap.getTerminalService().nextLine();
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        @NotNull final List<Task> unsortedTasks = taskEndpoint.findAllTasksByUserId(bootstrap.getSession());
        @NotNull final List<Task> sortedTasks = new LinkedList<>();
        switch (option) {
            case "createTime" :
                sortedTasks.addAll(taskEndpoint.sortTasksByCreateTime(bootstrap.getSession(), unsortedTasks, 1));
                break;
            case "startDate" :
                sortedTasks.addAll(taskEndpoint.sortTasksByCreateTime(bootstrap.getSession(), unsortedTasks, 1));
                break;
            case "finishDate" :
                sortedTasks.addAll(taskEndpoint.sortTasksByCreateTime(bootstrap.getSession(), unsortedTasks, 1));
                break;
            case "status" :
                sortedTasks.addAll(taskEndpoint.sortTasksByCreateTime(bootstrap.getSession(), unsortedTasks, 1));
        }
        printTasks(sortedTasks);
        System.out.println("[OK]");
    }
}

package ru.pyshinskiy.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataFasterXmlJSONSaveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_fasterXml_json_save";
    }

    @Override
    @NotNull
    public String description() {
        return "save data by fasterXml from json format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA FASTERXML JSON SAVE");
        bootstrap.getUserEndpoint().saveDataJsonByFasterXml(bootstrap.getSession());
        System.out.println("[OK]");
    }
}

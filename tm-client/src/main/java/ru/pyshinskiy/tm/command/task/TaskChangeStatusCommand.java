package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.Status;
import ru.pyshinskiy.tm.api.endpoint.Task;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskChangeStatusCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_change_status";
    }

    @Override
    @NotNull
    public String description() {
        return "change task status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CHANGE STATUS]");
        System.out.println("ENTER TASK ID");
        printTasks(bootstrap.getTaskEndpoint().findAllTasksByUserId(bootstrap.getSession()));
        final int taskNumber = Integer.parseInt(bootstrap.getTerminalService().nextLine()) - 1;
        @NotNull final String taskId = bootstrap.getTaskEndpoint().getTaskIdByNumber(bootstrap.getSession(), taskNumber);
        @Nullable final Task task = bootstrap.getTaskEndpoint().findOneTaskByUserId(bootstrap.getSession(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        System.out.println("Current task status is " + task.getStatus());
        System.out.println("ENTER NEW STATUS");
        @NotNull final Status newStatus = Status.valueOf(bootstrap.getTerminalService().nextLine());
        task.setStatus(newStatus);
        bootstrap.getTaskEndpoint().mergeTask(bootstrap.getSession(), task);
        System.out.println("[OK]");
    }
}

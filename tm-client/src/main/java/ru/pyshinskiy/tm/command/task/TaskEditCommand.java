package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Task;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_edit";
    }

    @Override
    @NotNull
    public String description() {
        return "edit existing task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskEndpoint.findAllTasksByUserId(bootstrap.getSession()));
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = taskEndpoint.getTaskIdByNumber(bootstrap.getSession(), taskNumber);
        @Nullable final Task task = taskEndpoint.findOneTaskByUserId(bootstrap.getSession(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        System.out.println("[ENTER TASK NAME]");
        @NotNull final Task anotherTask = new Task();
        anotherTask.setName(terminalService.nextLine());
        anotherTask.setId(task.getId());
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherTask.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER FINISH DATE");
        anotherTask.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        taskEndpoint.mergeTask(bootstrap.getSession(), anotherTask);
        System.out.println("[OK]");
    }
}

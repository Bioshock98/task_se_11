package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Project;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, finishDate, status]");
        @NotNull final String option = bootstrap.getTerminalService().nextLine();
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        @NotNull final List<Project> unsortedProjects = projectEndpoint.findAllProjectsByUserId(bootstrap.getSession());
        @NotNull final List<Project> sortedProjects = new LinkedList<>();
        switch (option) {
            case "createTime":
                sortedProjects.addAll(projectEndpoint.sortProjectsByCreateTime(bootstrap.getSession(), unsortedProjects, 1));
                break;
            case "startDate":
                sortedProjects.addAll(projectEndpoint.sortProjectsByStartDate(bootstrap.getSession(), unsortedProjects, 1));
                break;
            case "finishDate":
                sortedProjects.addAll(projectEndpoint.sortProjectsByFinishDate(bootstrap.getSession(), unsortedProjects, 1));
                break;
            case "status":
                sortedProjects.addAll(projectEndpoint.sortProjectsByStatus(bootstrap.getSession(), unsortedProjects, 1));
        }
        printProjects(sortedProjects);
        System.out.println("[OK]");
    }
}

package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Project;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSession() == null) return false;
        return bootstrap.getSession().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("DO YOU WANT TO SORT PROJECTS?");
        @NotNull final String doSort = bootstrap.getTerminalService().nextLine();
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        if("y".equals(doSort) || "yes".equals(doSort)) {
            System.out.println("CHOOSE SORT TYPE");
            System.out.println("[createTime, startDate, finishDate, status]");
            @NotNull final String option = bootstrap.getTerminalService().nextLine();
            @NotNull final List<Project> unsortedProjects = projectEndpoint.findAllProjects(bootstrap.getSession());
            @NotNull final List<Project> sortedProjects = new LinkedList<>();
            switch (option) {
                case "createTime" :
                    sortedProjects.addAll(projectEndpoint.sortProjectsByCreateTime(bootstrap.getSession(), unsortedProjects, 1));
                    break;
                case "startDate" :
                    sortedProjects.addAll(projectEndpoint.sortProjectsByStartDate(bootstrap.getSession(), unsortedProjects, 1));
                    break;
                case "finishDate" :
                    sortedProjects.addAll(projectEndpoint.sortProjectsByFinishDate(bootstrap.getSession(), unsortedProjects, 1));
                    break;
                case "status" :
                    sortedProjects.addAll(projectEndpoint.sortProjectsByStatus(bootstrap.getSession(), unsortedProjects, 1));
            }
            printProjects(sortedProjects);
            System.out.println("[OK]");
        }
        else {
            printProjects(projectEndpoint.findAllProjects(bootstrap.getSession()));
        }
    }
}

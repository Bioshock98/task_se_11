package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Project;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectEditAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSession() == null) return false;
        return bootstrap.getSession().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_edit_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "edit any project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectEndpoint.findAllProjects(bootstrap.getSession()));
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projectEndpoint.getProjectIdByNumber(bootstrap.getSession(), projectNumber);
        @Nullable final Project project = projectEndpoint.findOneProject(bootstrap.getSession(), projectId);
        @NotNull final Project anotherProject = new Project();
        anotherProject.setUserId(project.getUserId());
        System.out.println("ENTER NAME");
        anotherProject.setName(terminalService.nextLine());
        anotherProject.setId(project.getId());
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherProject.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER FINISH DATE");
        anotherProject.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        projectEndpoint.mergeProject(bootstrap.getSession(), anotherProject);
        System.out.println("[OK]");
    }
}

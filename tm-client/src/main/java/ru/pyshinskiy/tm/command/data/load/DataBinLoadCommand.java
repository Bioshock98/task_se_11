package ru.pyshinskiy.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataBinLoadCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_bin_load";
    }

    @Override
    @NotNull
    public String description() {
        return "load data from binary format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA BIN LOAD");
        bootstrap.getUserEndpoint().loadDataBin(bootstrap.getSession());
        System.out.println("[OK]");
    }
}

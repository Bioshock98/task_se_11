package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Project;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProject;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectSelectCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_select";
    }

    @Override
    @NotNull
    public String description() {
        return "show project info";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        System.out.println("[PROJECT SELECT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectEndpoint.findAllProjectsByUserId(bootstrap.getSession()));
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projectEndpoint.getProjectIdByNumber(bootstrap.getSession(), projectNumber);
        @Nullable final Project project = projectEndpoint.findOneProjectByUserId(bootstrap.getSession(), projectId);
        if(project == null) {
            throw new Exception("project doesn't exist");
        }
        printProject(project);
        System.out.println("[OK]");
    }
}

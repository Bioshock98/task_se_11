package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.Task;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSession() == null) return false;
        return bootstrap.getSession().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST ADMIN]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = bootstrap.getTerminalService().nextLine();
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        @NotNull final List<Task> unsortedTasks = taskEndpoint.findAllTasks(bootstrap.getSession());
        @NotNull final List<Task> sortedTasks = new LinkedList<>();
        switch (option) {
            case "createTime" :
                sortedTasks.addAll(taskEndpoint.sortTasksByCreateTime(bootstrap.getSession(), unsortedTasks, 1));
                break;
            case "startDate" :
                sortedTasks.addAll(taskEndpoint.sortTasksByStartDate(bootstrap.getSession(), unsortedTasks, 1));
                break;
            case "finishDate" :
                sortedTasks.addAll(taskEndpoint.sortTasksByFinishDate(bootstrap.getSession(), unsortedTasks, 1));
                break;
            case "status" :
                sortedTasks.addAll(taskEndpoint.sortTasksByStatus(bootstrap.getSession(), unsortedTasks, 1));
        }
        printTasks(sortedTasks);
        System.out.println("[OK]");
    }
}

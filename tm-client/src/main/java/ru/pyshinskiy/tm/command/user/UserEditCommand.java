package ru.pyshinskiy.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.User;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class UserEditCommand extends AbstractCommand {
    
    @Override
    @NotNull
    public String command() {
        return "user_edit";
    }

    @Override
    @NotNull
    public String description() {
        return "edit existing user";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IUserEndpoint userEndpoint = bootstrap.getUserEndpoint();
        @NotNull final String userId = bootstrap.getSession().getUserId();
        @NotNull final String currentUserPassword = userEndpoint.findOneUser(bootstrap.getSession(), userId).getPassword();
        System.out.println("[USER EDIT]");
        System.out.println("ENTER PASSWORD");
        while(!DigestUtils.md5Hex(terminalService.nextLine()).equals(currentUserPassword)) {
            System.out.println("Incorrect password");
        }
        System.out.println("ENTER NEW USERNAME");
        @NotNull final User user = new User();
        user.setId(bootstrap.getSession().getUserId());
        user.setLogin(terminalService.nextLine());
        System.out.println("ENTER NEW PASSWORD");
        user.setPassword(terminalService.nextLine());
        bootstrap.getUserEndpoint().mergeUser(bootstrap.getSession(), user);
        System.out.println("[OK]");
    }
}

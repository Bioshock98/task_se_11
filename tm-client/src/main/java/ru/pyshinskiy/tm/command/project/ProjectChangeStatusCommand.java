package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Project;
import ru.pyshinskiy.tm.api.endpoint.Status;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectChangeStatusCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_change_status";
    }

    @Override
    @NotNull
    public String description() {
        return "change project status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CHANGE STATUS]");
        System.out.println("ENTER PROJECT ID");
        printProjects(bootstrap.getProjectEndpoint().findAllProjectsByUserId(bootstrap.getSession()));
        @NotNull final int projectNumber = Integer.parseInt(bootstrap.getTerminalService().nextLine()) - 1;
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        @NotNull final String projectId = projectEndpoint.getProjectIdByNumber(bootstrap.getSession(), projectNumber);
        @Nullable final Project project = projectEndpoint.findOneProjectByUserId(bootstrap.getSession(), projectId);
        if(project == null) {
            throw new Exception("project doesn't exist");
        }
        System.out.println("Current project status is " + project.getStatus());
        System.out.println("ENTER NEW STATUS");
        @NotNull final Status newStatus = Status.valueOf(bootstrap.getTerminalService().nextLine());
        project.setStatus(newStatus);
        bootstrap.getProjectEndpoint().mergeProject(bootstrap.getSession(), project);
        System.out.println("[OK]");
    }
}

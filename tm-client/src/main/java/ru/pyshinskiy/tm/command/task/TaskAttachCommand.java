package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Project;
import ru.pyshinskiy.tm.api.endpoint.Task;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskAttachCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_attach";
    }

    @Override
    @NotNull
    public String description() {
        return "attach task to project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        System.out.println("[ATTACH TASK]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectEndpoint.findAllProjectsByUserId(bootstrap.getSession()));
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projectEndpoint.getProjectIdByNumber(bootstrap.getSession(), projectNumber);
        @Nullable final Project project = projectEndpoint.findOneProjectByUserId(bootstrap.getSession(), projectId);
        if(project == null) {
            throw new Exception("project doesn't exist");
        }
        System.out.println("ENTER TASK ID");
        printTasks(taskEndpoint.findAllTasksByUserId(bootstrap.getSession()));
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = taskEndpoint.getTaskIdByNumber(bootstrap.getSession(), taskNumber);
        @Nullable final Task task = taskEndpoint.findOneTaskByUserId(bootstrap.getSession(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        task.setProjectId(project.getId());
        taskEndpoint.mergeTask(bootstrap.getSession(), task);
        System.out.println("[OK]");
    }
}

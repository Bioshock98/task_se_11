package ru.pyshinskiy.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataBinSaveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_bin_save";
    }

    @Override
    public @NotNull String description() {
        return "save data in binary format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA BIN SAVE");
        bootstrap.getUserEndpoint().saveDataBin(bootstrap.getSession());
        System.out.println("[OK]");
    }
}

package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskRemoveAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSession() == null) return false;
        return bootstrap.getSession().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_remove_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "remove any user task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER PROJECT ID");
        System.out.println("ENTER TASK ID");
        printTasks(taskEndpoint.findAllTasks(bootstrap.getSession()));
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = taskEndpoint.getTaskIdByNumber(bootstrap.getSession(), taskNumber);
        taskEndpoint.removeTask(bootstrap.getSession(), taskId);
        System.out.println("[TASK REMOVED]");
    }
}

package ru.pyshinskiy.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataJaxBXmlLoadCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_jaxB_xml_load";
    }

    @Override
    @NotNull
    public String description() {
        return "load data by jaxB from xml format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA JAXB XML LOAD");
        bootstrap.getUserEndpoint().loadDataXmlByJaxB(bootstrap.getSession());
        System.out.println("[OK]");
    }
}

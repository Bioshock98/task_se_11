package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.Task;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskEditAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSession() == null) return false;
        return bootstrap.getSession().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_edit_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "edit any task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskEndpoint.findAllTasks(bootstrap.getSession()));
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = taskEndpoint.getTaskIdByNumber(bootstrap.getSession(), taskNumber);
        @Nullable final Task task = taskEndpoint.findOneTask(bootstrap.getSession(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        System.out.println("[ENTER TASK NAME]");
        @NotNull final Task anotherTask = new Task();
        anotherTask.setUserId(task.getUserId());
        anotherTask.setName(terminalService.nextLine());
        anotherTask.setId(task.getId());
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherTask.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER FINISH DATE");
        anotherTask.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("[OK]");
        taskEndpoint.mergeTask(bootstrap.getSession(), anotherTask);
    }
}

package ru.pyshinskiy.tm.util.entity;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.Project;
import ru.pyshinskiy.tm.api.endpoint.Task;
import ru.pyshinskiy.tm.api.endpoint.User;

import java.util.List;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateToString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toDate;

public final class EntityUtil {

    public static void printProjects(@NotNull final List<Project> projects) {
        for (int i = 0; i < projects.size(); i++) {
            @NotNull final Project project = projects.get(i);
            System.out.println((i + 1) + "." + " " + project.getName());
        }
    }

    public static void printProject(@NotNull final Project project) {
        if(project.getStartDate() == null) {
            System.out.println("project start date is null");
            return;
        }
        if(project.getFinishDate() == null) {
            System.out.println("project end date is null");
            return;
        }
        @NotNull final StringBuilder formatedProject = new StringBuilder();
        formatedProject.append("project name: ");
        formatedProject.append(project.getName());
        formatedProject.append("\nproject description: ");
        formatedProject.append(project.getDescription());
        formatedProject.append("\nproject status: ");
        formatedProject.append(project.getStatus());
        formatedProject.append("\nstart date: ");
        formatedProject.append(parseDateToString(toDate(project.getStartDate())));
        formatedProject.append("\nend date: ");
        formatedProject.append(parseDateToString(toDate(project.getStartDate())));
        System.out.println(formatedProject);
    }

    public static void printTasks(@NotNull final List<Task> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i + 1) + "." + " " + tasks.get(i).getName());
        }
    }

    public static void printTask(@NotNull final Task task) {
        if(task.getStartDate() == null) {
            System.out.println("project start date is null");
            return;
        }
        if(task.getFinishDate() == null) {
            System.out.println("project end date is null");
            return;
        }
        @NotNull final StringBuilder formatedTask = new StringBuilder();
        formatedTask.append("task name: ");
        formatedTask.append(task.getName());
        formatedTask.append("\ntask description: ");
        formatedTask.append(task.getDescription());
        formatedTask.append("\ntask status: ");
        formatedTask.append(task.getStatus());
        formatedTask.append("\nstart date: ");
        formatedTask.append(parseDateToString(toDate(task.getStartDate())));
        formatedTask.append("\nend date: ");
        formatedTask.append(parseDateToString(toDate(task.getFinishDate())));
        System.out.println(formatedTask);
    }

    public static void printUsers(@NotNull final List<User> users) {
        for(int i = 0; i < users.size(); i++) {
            System.out.println((i + 1) + ". username: " + users.get(i).getLogin());
            System.out.println("role: " + users.get(i).getRole());
        }
    }

    public static void printUser(@NotNull final User user) {
        System.out.println("username: " + user.getLogin());
        System.out.println("role: " + user.getRole());
    }
}

package ru.pyshinskiy.tm.util.date;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtil {

    @NotNull
    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static Date parseDateFromString(@NotNull final String stringDate) {
        return dateFormatter.parse(stringDate, new ParsePosition(0));
    }

    @NotNull
    public static String parseDateToString(@NotNull final Date date) {
        return dateFormatter.format(date);
    }

    @Nullable
    public static XMLGregorianCalendar toXMLGregorianCalendar(@NotNull final Date date) throws Exception {
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        return xmlCalendar;
    }

    @Nullable
    public static Date toDate(@Nullable final XMLGregorianCalendar calendar){
        if(calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }
}

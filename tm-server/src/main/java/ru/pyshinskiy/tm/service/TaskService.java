package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskRepository;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public final class TaskService extends AbstractWBSService<Task> implements ITaskService {

    @NotNull private final ITaskRepository taskRepository = (ITaskRepository) wbsRepository;

    public TaskService(@NotNull final ITaskRepository abstractRepository) {
        super(abstractRepository);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if(userId == null || projectId == null) throw new Exception("one of the parameters passed is zero");
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if(projectId == null || projectId.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        taskRepository.removeAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAttachedTasks() throws Exception {
        taskRepository.removeAttachedTasks();
    }

    @Override
    public void removeAttachedTasks(@NotNull final String userId) throws Exception {
        taskRepository.removeAttachedTasks(userId);
    }
}

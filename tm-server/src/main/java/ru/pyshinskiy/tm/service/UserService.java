package ru.pyshinskiy.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.repository.IRepository;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.api.user.IUserRepository;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.constant.AppConst;
import ru.pyshinskiy.tm.dto.Domain;
import ru.pyshinskiy.tm.entity.User;

import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final IUserRepository userRepository = (IUserRepository) abstractRepository;
    @NotNull private final IProjectService projectService;
    @NotNull private final ITaskService taskService;

    public UserService(@NotNull final IRepository<User> abstractRepository, @NotNull final IProjectService projectService,
                       @NotNull final ITaskService taskService) {
        super(abstractRepository);
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    @Nullable
    public User getUserByLogin(@Nullable final String login) throws Exception {
        if(login == null) return null;
        return userRepository.getUserByLogin(login);
    }

    @Override
    public void loadDataBin() throws Exception {
        @NotNull final File file = new File(AppConst.SAVE_DIR + "data.bin");
        @NotNull final FileInputStream inputStream = new FileInputStream(file);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        projectService.persist(domain.getProjects());
        taskService.persist(domain.getTasks());
        userRepository.persist(domain.getUsers());
    }

    @Override
    public void loadDataJsonByFasterXml() throws Exception {
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.json");
        savePath.getParentFile().mkdirs();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Domain domain = mapper.readValue(savePath, Domain.class);
        projectService.persist(domain.getProjects());
        taskService.persist(domain.getTasks());
        userRepository.persist(domain.getUsers());
    }

    @Override
    public void loadDataXmlByFasterXml() throws Exception {
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.xml");
        savePath.getParentFile().mkdirs();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savePath, Domain.class);
        projectService.persist(domain.getProjects());
        taskService.persist(domain.getTasks());
        userRepository.persist(domain.getUsers());
    }

    @Override
    public void loadDataJsonByJaxB() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.json");
        savePath.getParentFile().mkdirs();
        @NotNull final StreamSource streamSource = new StreamSource(savePath);
        @NotNull final JAXBContext jc = (JAXBContext) JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jc.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = unmarshaller.unmarshal(streamSource, Domain.class).getValue();
        projectService.persist(domain.getProjects());
        taskService.persist(domain.getTasks());
        userRepository.persist(domain.getUsers());
    }

    @Override
    public void loadDataXmlByJaxB() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.xml");
        savePath.getParentFile().mkdirs();
        @NotNull final StreamSource streamSource = new StreamSource(savePath);
        @NotNull final JAXBContext jc = (JAXBContext) JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jc.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/xml");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = unmarshaller.unmarshal(streamSource, Domain.class).getValue();
        projectService.persist(domain.getProjects());
        taskService.persist(domain.getTasks());
        userRepository.persist(domain.getUsers());
    }

    @Override
    public void saveDataBin(@NotNull final String userId) throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(projectService, taskService, this, userId);
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.bin");
        savePath.getParentFile().mkdirs();
        System.out.println(savePath);
        @NotNull final FileOutputStream outputStream = new FileOutputStream(savePath);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    @Override
    public void saveDataJsonByFasterXml(@NotNull final String userId) throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(projectService, taskService, this, userId);
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.json");
        savePath.getParentFile().mkdirs();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(savePath, domain);
    }

    @Override
    public void saveDataXmlByFasterXml(@NotNull final String userId) throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(projectService, taskService, this, userId);
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.xml");
        savePath.getParentFile().mkdirs();
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(savePath, domain);
    }

    @Override
    public void saveDataJsonByJaxB(@NotNull final String userId) throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(projectService, taskService, this, userId);
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.json");
        savePath.getParentFile().mkdirs();
        @NotNull final FileOutputStream outputStream = new FileOutputStream(savePath);
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jc = (JAXBContext) JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, outputStream);
    }

    @Override
    public void saveDataXmlByJaxB(@NotNull final String userId) throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(projectService, taskService, this, userId);
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.xml");
        savePath.getParentFile().mkdirs();
        @NotNull final FileOutputStream outputStream = new FileOutputStream(savePath);
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jc = (JAXBContext) JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/xml");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, outputStream);
    }

    @Override
    @Nullable
    public String getIdByNumber(int number) throws Exception {
        return userRepository.getIdByNumber(number);
    }
}

package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.IRepository;
import ru.pyshinskiy.tm.api.service.IService;
import ru.pyshinskiy.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull protected final IRepository<T> abstractRepository;

    public AbstractService(@NotNull final IRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Nullable
    @Override
    public T findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("id is empty or null");
        return abstractRepository.findOne(id);
    }

    @NotNull
    @Override
    public List<T> findAll() throws Exception {
        return abstractRepository.findAll();
    }

    @Nullable
    @Override
    public T persist(@Nullable final T t) throws Exception {
        if(t == null) throw new Exception("entity is null");
        return abstractRepository.persist(t);
    }

    @NotNull
    @Override
    public List<T> persist(@NotNull final List<T> ts) throws Exception {
        return abstractRepository.persist(ts);
    }

    @Nullable
    @Override
    public T merge(@Nullable final T t) throws Exception {
        if(t == null) throw new Exception("entity is null");
        return abstractRepository.merge(t);
    }

    @Nullable
    @Override
    public T remove(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("id is empty or null");
        return abstractRepository.remove(id);
    }

    @Override
    public void removeAll() throws Exception {
        abstractRepository.removeAll();
    }
}

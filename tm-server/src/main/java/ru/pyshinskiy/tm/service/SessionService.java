package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.IRepository;
import ru.pyshinskiy.tm.api.session.ISessionRepository;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.entity.Session;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull private final ISessionRepository sessionRepository = (ISessionRepository) abstractRepository;

    public SessionService(@NotNull final IRepository<Session> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    @Nullable
    public Session findOne(@Nullable final String userId, @Nullable final String id) throws Exception{
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        return sessionRepository.findOne(userId, id);
    }

    @Override
    public void remove(@Nullable String userId, @Nullable String id) throws Exception {
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        sessionRepository.remove(userId, id);
    }
}

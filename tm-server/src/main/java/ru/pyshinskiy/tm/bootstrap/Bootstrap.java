package ru.pyshinskiy.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.endpoint.ProjectEndpoint;
import ru.pyshinskiy.tm.endpoint.SessionEndpoint;
import ru.pyshinskiy.tm.endpoint.TaskEndpoint;
import ru.pyshinskiy.tm.endpoint.UserEndpoint;
import ru.pyshinskiy.tm.repository.ProjectRepository;
import ru.pyshinskiy.tm.repository.SessionRepository;
import ru.pyshinskiy.tm.repository.TaskRepository;
import ru.pyshinskiy.tm.repository.UserRepository;
import ru.pyshinskiy.tm.service.ProjectService;
import ru.pyshinskiy.tm.service.SessionService;
import ru.pyshinskiy.tm.service.TaskService;
import ru.pyshinskiy.tm.service.UserService;

import javax.xml.ws.Endpoint;

@NoArgsConstructor
public class Bootstrap {

    @NotNull private final IProjectService projectService = new ProjectService(new ProjectRepository());

    @NotNull private final ITaskService taskService = new TaskService(new TaskRepository());

    @NotNull private final IUserService userService = new UserService(new UserRepository(), projectService, taskService);

    @NotNull private final ISessionService sessionService = new SessionService(new SessionRepository());

    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(sessionService, projectService);

    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, taskService);

    @NotNull private final IUserEndpoint userEndpoint = new UserEndpoint(sessionService, userService);

    @NotNull private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService, userService);

    public void start() throws Exception {
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskservice?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userservice?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionservice?wsdl", sessionEndpoint);
    }
}

package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.session.ISessionRepository;
import ru.pyshinskiy.tm.entity.Session;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Nullable
    public Session findOne(@NotNull final String userId, @NotNull final String id) {
        for(@NotNull final Session session : entityMap.values()) {
            if(id.equals(session.getId()) && userId.equals(session.getUserId())) return session;
        }
        return null;
    }

    @Override
    public void remove(@NotNull String userId, @NotNull String id) throws Exception {
        for(@NotNull final Session session : entityMap.values()) {
            if(id.equals(session.getId()) && userId.equals(session.getUserId())) entityMap.remove(id);
        }
    }
}

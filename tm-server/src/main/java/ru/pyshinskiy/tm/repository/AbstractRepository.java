package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.IRepository;
import ru.pyshinskiy.tm.entity.AbstractEntity;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull protected final Map<String, T> entityMap = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<T> findAll() throws Exception {
        return new LinkedList<>(entityMap.values());
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String id) throws Exception {
        return entityMap.get(id);
    }

    @Nullable
    @Override
    public T persist(@NotNull final T t) throws Exception {
        return entityMap.put(t.getId(), t);
    }

    @NotNull
    @Override
    public List<T> persist(@NotNull final List<T> ts) {
        for(T t : ts) {
            entityMap.put(t.getId(), t);
        }
        return ts;
    }

    @Nullable
    @Override
    public T merge(@NotNull final T t) throws Exception {
        return entityMap.put(t.getId(), t);
    }

    @Nullable
    @Override
    public T remove(@NotNull final String id) throws Exception {
        return entityMap.remove(id);
    }

    @Override
    public void removeAll() throws Exception {
        entityMap.clear();
    }
}

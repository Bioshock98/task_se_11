package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.user.IUserRepository;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User getUserByLogin(@NotNull final String login) throws Exception {
        for(@NotNull final User user : findAll()) {
            if(login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public String getIdByNumber(final int number) throws Exception {
        @NotNull final List<User> users = findAll();
        for(@NotNull final User user : findAll()) {
            if(user.getId().equals(users.get(number).getId())) return user.getId();
        }
        return null;
    }
}

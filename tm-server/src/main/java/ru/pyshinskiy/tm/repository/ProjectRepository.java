package ru.pyshinskiy.tm.repository;

import ru.pyshinskiy.tm.api.project.IProjectRepository;
import ru.pyshinskiy.tm.entity.Project;

public final class ProjectRepository extends AbstractWBSRepository<Project> implements IProjectRepository {
}

package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.task.ITaskRepository;
import ru.pyshinskiy.tm.entity.Task;

import java.util.LinkedList;
import java.util.List;

public final class TaskRepository extends AbstractWBSRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final LinkedList<Task> projectTasks = new LinkedList<>();
        for(@NotNull final Task task : findAll(userId)) {
            if(task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        for(@NotNull final Task task : findAll(userId)) {
            if(projectId.equals(task.getProjectId())) {
                remove(userId, task.getId());
            }
        }
    }

    @Override
    public void removeAttachedTasks() throws Exception {
        for(@NotNull final Task task : findAll()) {
            if(task.getProjectId() != null) remove(task.getId());
        }
    }

    @Override
    public void removeAttachedTasks(@NotNull final String userId) throws Exception {
        for(@NotNull final Task task : findAll(userId)) {
            if(task.getProjectId() != null) remove(task.getId());
        }
    }
}

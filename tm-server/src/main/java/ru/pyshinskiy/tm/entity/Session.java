package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumerated.Role;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class Session extends AbstractEntity {

    @NotNull String userId;

    @NotNull Role role;

    @NotNull Date createDate = new Date(System.currentTimeMillis());

    @Nullable String signature;
}

package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
public final class Task extends AbstractWBS implements Serializable {

    @Nullable
    private String projectId;
}

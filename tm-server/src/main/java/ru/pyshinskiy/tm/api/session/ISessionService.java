package ru.pyshinskiy.tm.api.session;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;
import ru.pyshinskiy.tm.entity.Session;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session findOne(@Nullable final String userId, @Nullable final String id) throws Exception;

    void remove(@Nullable final String userId, @Nullable final String id) throws Exception;
}

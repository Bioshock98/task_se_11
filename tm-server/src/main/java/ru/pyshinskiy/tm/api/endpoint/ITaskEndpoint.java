package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    @Nullable
    Task findOneTask(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    @NotNull
    List<Task> findAllTasks(@Nullable final Session session) throws Exception;

    @WebMethod
    @Nullable
    Task persistTask(@Nullable final Session session, @Nullable final Task t) throws Exception;

    @WebMethod
    @Nullable
    List<Task> persistTasks(@Nullable final Session session, @Nullable final List<Task> ts) throws Exception;

    @WebMethod
    @Nullable
    Task mergeTask(@Nullable final Session session, @Nullable final Task t) throws Exception;

    @WebMethod
    @Nullable
    Task removeTask(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllTasks(@Nullable final Session session) throws Exception;

    @WebMethod
    @Nullable
    String getTaskIdByNumber(@Nullable final Session session, final int number) throws Exception;

    @WebMethod
    @NotNull
    List<Task> findAllTasksByUserId(@Nullable final Session session) throws Exception;

    @WebMethod
    @Nullable
    Task findOneTaskByUserId(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    @Nullable
    List<Task> findTasksByName(@Nullable final Session session, @Nullable final String name) throws Exception;

    @WebMethod
    @Nullable
    List<Task> findTasksByDescription(@Nullable final Session session, @Nullable final String description) throws Exception;

    @WebMethod
    @Nullable
    Task removeTaskByUserId(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllTasksByUserId(@Nullable final Session session) throws Exception;

    @WebMethod
    @NotNull
    List<Task> sortTasksByCreateTime(@Nullable final Session session, @NotNull final List<Task> objectives, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<Task> sortTasksByStartDate(@Nullable final Session session, @NotNull final List<Task> objectives, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<Task> sortTasksByFinishDate(@Nullable final Session session, @NotNull final List<Task> objectives, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<Task> sortTasksByStatus(@Nullable final Session session, @NotNull final List<Task> objectives, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<Task> findTasksByProjectId(@Nullable final Session session, @Nullable final String projectId) throws Exception;

    @WebMethod
    void removeTasksByProjectId(@Nullable final Session session, @Nullable final String projectId) throws Exception;

    @WebMethod
    void removeAllAttachedTasks(@Nullable final Session session) throws Exception;

    @WebMethod
    void removeAllAttachedTasksByUserId(@Nullable final Session session) throws Exception;
}

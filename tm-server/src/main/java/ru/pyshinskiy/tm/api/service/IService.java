package ru.pyshinskiy.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @Nullable
    T findOne(@Nullable final String id) throws Exception;

    @NotNull
    List<T> findAll() throws Exception;

    @Nullable
    T persist(@Nullable final T t) throws Exception;

    @NotNull
    List<T> persist(@NotNull final List<T> ts) throws Exception;

    @Nullable
    T merge(@Nullable final T t) throws Exception;

    @Nullable
    T remove(@Nullable final String id) throws Exception;

    void removeAll() throws Exception;
}

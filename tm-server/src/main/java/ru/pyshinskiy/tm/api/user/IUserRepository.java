package ru.pyshinskiy.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.IRepository;
import ru.pyshinskiy.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User getUserByLogin(@NotNull final String login) throws Exception;

    @Nullable
    String getIdByNumber(final int number) throws Exception;
}

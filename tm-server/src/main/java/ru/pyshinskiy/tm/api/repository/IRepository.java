package ru.pyshinskiy.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {

    @NotNull
    List<T> findAll() throws Exception;

    @Nullable
    T findOne(@NotNull final String id) throws Exception;

    @Nullable
    T persist(@NotNull final T t) throws Exception;

    @NotNull
    List<T> persist(@NotNull final List<T> ts) throws Exception;

    @Nullable
    T merge(@NotNull final T t) throws Exception;

    @Nullable
    T remove(@NotNull final String id) throws Exception;

    void removeAll() throws Exception;
}

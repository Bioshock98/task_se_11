package ru.pyshinskiy.tm.api.wbs;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.IRepository;

import java.util.List;

public interface IAbstractWBSRepository<T> extends IRepository<T> {

    @NotNull
    List<T> findAll(@NotNull final String userId) throws Exception;

    @Nullable
    T findOne(@NotNull final String userId, @NotNull final String id) throws Exception;

    @Nullable
    List<T> findByName(@NotNull final String userId, @NotNull final String name) throws Exception;

    @Nullable
    List<T> findByDescription(@NotNull final String userId, @NotNull final String description) throws Exception;

    @Nullable
    T remove(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAll(@NotNull final String userId) throws Exception;

    @NotNull
    List<T> sortByCreateTime(@NotNull final List<T> objectives, final int direction) throws Exception;

    @NotNull
    List<T> sortByStartDate(@NotNull final List<T> objectives, final int direction) throws Exception;

    @NotNull
    List<T> sortByFinishDate(@NotNull final List<T> objectives, final int direction) throws Exception;

    @NotNull
    List<T> sortByStatus(@NotNull final List<T> objectives, final int direction) throws Exception;

    @Nullable
    String getIdByNumber(final int number) throws Exception;
}

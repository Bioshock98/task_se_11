package ru.pyshinskiy.tm.api.project;

import ru.pyshinskiy.tm.api.wbs.IAbstractWBSRepository;
import ru.pyshinskiy.tm.entity.Project;

public interface IProjectRepository extends IAbstractWBSRepository<Project> {
}

package ru.pyshinskiy.tm.api.wbs;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;

import java.util.List;

public interface IAbstractVBSService<T> extends IService<T> {

    @NotNull
    List<T> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    T findOne(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    List<T> findByName(@Nullable final String userId, @Nullable final String name) throws Exception;

    @Nullable
    List<T> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception;

    @Nullable
    T remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    @NotNull
    List<T> sortByCreateTime(@NotNull final List<T> objectives, final int direction) throws Exception;

    @NotNull
    List<T> sortByStartDate(@NotNull final List<T> objectives, final int direction) throws Exception;

    @NotNull
    List<T> sortByFinishDate(@NotNull final List<T> objectives, final int direction) throws Exception;

    @NotNull
    List<T> sortByStatus(@NotNull final List<T> objectives, final int direction) throws Exception;

    @Nullable
    String getIdByNumber(final int number) throws Exception;
}

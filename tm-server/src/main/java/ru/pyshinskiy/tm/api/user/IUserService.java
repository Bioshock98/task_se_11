package ru.pyshinskiy.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;
import ru.pyshinskiy.tm.entity.User;

public interface IUserService extends IService<User> {

    @Nullable
    User getUserByLogin(@Nullable final String login) throws Exception;

    void loadDataBin() throws Exception;

    void loadDataJsonByFasterXml() throws Exception;

    void loadDataXmlByFasterXml() throws Exception;

    void loadDataJsonByJaxB() throws Exception;

    void loadDataXmlByJaxB() throws Exception;

    void saveDataBin(@NotNull final String userId) throws Exception;

    void saveDataJsonByFasterXml(@NotNull final String userId) throws Exception;

    void saveDataXmlByFasterXml(@NotNull final String userId) throws Exception;

    void saveDataJsonByJaxB(@NotNull final String userId) throws Exception;

    void saveDataXmlByJaxB(@NotNull final String userId) throws Exception;

    @Nullable
    String getIdByNumber(final int number) throws Exception;
}

package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    @NotNull
    List<Project> findAllProjects(@Nullable final Session session) throws Exception;

    @WebMethod
    @Nullable
    Project findOneProject(@Nullable final Session session, @NotNull final String id) throws Exception;

    @WebMethod
    @NotNull
    List<Project> findAllProjectsByUserId(@Nullable final Session session) throws Exception;

    @WebMethod
    @Nullable
    Project findOneProjectByUserId(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    @Nullable
    Project persistProject(@Nullable final Session session, @Nullable final Project t) throws Exception;

    @WebMethod
    @Nullable
    List<Project> persistProjects(@Nullable final Session session, @Nullable final List<Project> ts) throws Exception;

    @WebMethod
    @Nullable
    Project mergeProject(@Nullable final Session session, @NotNull final Project t) throws Exception;

    @WebMethod
    @Nullable
    Project removeProject(@Nullable final Session session, @NotNull final String id) throws Exception;

    @WebMethod
    @Nullable
    Project removeProjectByUserId(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllProjects(@Nullable final Session session) throws Exception;

    @WebMethod
    void removeAllProjectsByUserId(@Nullable final Session session) throws Exception;

    @WebMethod
    @Nullable
    String getProjectIdByNumber(@Nullable final Session session, final int number) throws Exception;

    @WebMethod
    @Nullable
    List<Project> findProjectByName(@Nullable final Session session, @Nullable final String name) throws Exception;

    @WebMethod
    @Nullable
    List<Project> findProjectByDescription(@Nullable final Session session, @Nullable final String description) throws Exception;

    @WebMethod
    @NotNull
    List<Project> sortProjectsByCreateTime(@Nullable final Session session, @NotNull final List<Project> projects, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<Project> sortProjectsByStartDate(@Nullable final Session session, @NotNull final List<Project> projects, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<Project> sortProjectsByFinishDate(@Nullable final Session session, @NotNull final List<Project> projects, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<Project> sortProjectsByStatus(@Nullable final Session session, @NotNull final List<Project> projects, final int direction) throws Exception;
}

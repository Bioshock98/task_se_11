package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    @Nullable
    User findOneUser(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    @NotNull
    List<User> findAllUsers(@Nullable final Session session) throws Exception;

    @WebMethod
    @Nullable
    User persistUser(@NotNull final String login, @NotNull final String password, @NotNull final Role role) throws Exception;

    @WebMethod
    @Nullable
    List<User> persistUsers(@Nullable final Session session, @Nullable final List<User> ts) throws Exception;

    @WebMethod
    @Nullable
    User mergeUser(@Nullable final Session session, @Nullable final User t) throws Exception;

    @WebMethod
    @Nullable
    User removeUser(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllUsers(@Nullable final Session session) throws Exception;

    @WebMethod
    @Nullable
    String getUserIdByNumber(@Nullable final Session session, final int number) throws Exception;

    @WebMethod
    @Nullable
    User getUserByLoginE(@Nullable final Session session, @Nullable final String login) throws Exception;

    @WebMethod
    void loadDataBin(@Nullable final Session session) throws Exception;

    @WebMethod
    void loadDataJsonByFasterXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void loadDataXmlByFasterXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void loadDataJsonByJaxB(@Nullable final Session session) throws Exception;

    @WebMethod
    void loadDataXmlByJaxB(@Nullable final Session session) throws Exception;

    @WebMethod
    void saveDataBin(@Nullable final Session session) throws Exception;

    @WebMethod
    void saveDataJsonByFasterXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void saveDataXmlByFasterXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void saveDataJsonByJaxB(@Nullable final Session session) throws Exception;

    @WebMethod
    void saveDataXmlByJaxB(@Nullable final Session session) throws Exception;
}

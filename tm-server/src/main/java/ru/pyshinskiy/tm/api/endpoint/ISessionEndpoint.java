package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    @Nullable
    Session createSession(@NotNull final String login, @NotNull final String password) throws Exception;

    @WebMethod
    void removeSession(@Nullable final String userId, @Nullable final String id) throws Exception;
}

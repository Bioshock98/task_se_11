package ru.pyshinskiy.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;

import static ru.pyshinskiy.tm.constant.AppConst.CICLE;
import static ru.pyshinskiy.tm.constant.AppConst.SALT;

@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull private IUserService userService;

    public SessionEndpoint() {
        super();
    }

    public SessionEndpoint(@NotNull final ISessionService sessionService, @NotNull final IUserService userService) {
        super(sessionService);
        this.userService = userService;
    }

    @WebMethod
    @Override
    @Nullable
    public Session createSession(@NotNull final String login, @NotNull final String password) throws Exception {
        @Nullable final User user = userService.getUserByLogin(login);
        if(user == null) return null;
        if(!user.getPassword().equals(SignatureUtil.sign(password, SALT, CICLE))) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session, SALT, CICLE));
        sessionService.persist(session);
        return session;
    }

    @WebMethod
    @Override
    public void removeSession(@Nullable final String userId, @Nullable final String id) throws Exception {
        sessionService.remove(userId, id);
    }
}

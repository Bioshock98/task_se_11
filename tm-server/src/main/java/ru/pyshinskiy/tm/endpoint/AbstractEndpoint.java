package ru.pyshinskiy.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.entity.Session;

import java.util.Date;

import static ru.pyshinskiy.tm.constant.AppConst.*;
import static ru.pyshinskiy.tm.util.SignatureUtil.sign;

public abstract class AbstractEndpoint {

    @NotNull ISessionService sessionService;

    public AbstractEndpoint() {

    }

    public AbstractEndpoint(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    protected void validateSession(@Nullable final Session userSession) throws Exception {
        if(userSession == null) throw new Exception("method is unavailable for unauthorized users");
        @Nullable final Session session = sessionService.findOne(userSession.getUserId(), userSession.getId());
        userSession.setSignature(null);
        if(session == null) throw new Exception("Session is doesn't exist");
        if(!session.getSignature().equals(sign(userSession, SALT, CICLE))) throw new Exception("Session is invalid");
        if(new Date().getTime() - userSession.getCreateDate().getTime() > SESSION_LIFE_TIME) throw new Exception("Session time out\n you need to log in again");
    }
}

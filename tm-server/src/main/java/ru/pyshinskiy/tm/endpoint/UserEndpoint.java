package ru.pyshinskiy.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;
import ru.pyshinskiy.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

import static ru.pyshinskiy.tm.constant.AppConst.CICLE;
import static ru.pyshinskiy.tm.constant.AppConst.SALT;

@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull private IUserService userService;

    public UserEndpoint() {

    }

    public UserEndpoint(@NotNull final ISessionService sessionService, @NotNull final IUserService userService) {
        super(sessionService);
        this.userService = userService;
    }

    @WebMethod
    @Nullable
    @Override
    public User findOneUser(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return userService.findOne(id);
    }

    @WebMethod
    @NotNull
    @Override
    public List<User> findAllUsers(@Nullable final Session session) throws Exception {
        validateSession(session);
        return userService.findAll();
    }

    @WebMethod
    @Nullable
    @Override
    public User persistUser(@NotNull final String login, @NotNull final String password, @NotNull final Role role) throws Exception {
        User user = new User();
        user.setLogin(login);
        user.setPassword(SignatureUtil.sign(password, SALT, CICLE));
        user.setRole(role);
        return userService.persist(user);
    }


    @WebMethod
    @NotNull
    @Override
    public List<User> persistUsers(@Nullable final Session session, @Nullable final List<User> ts) throws Exception {
        validateSession(session);
        return userService.persist(ts);
    }

    @WebMethod
    @Nullable
    @Override
    public User mergeUser(@Nullable final Session session, @Nullable final User t) throws Exception {
        validateSession(session);
        return userService.merge(t);
    }

    @WebMethod
    @Nullable
    @Override
    public User removeUser(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return userService.remove(id);
    }

    @WebMethod
    @Override
    public void removeAllUsers(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.removeAll();
    }

    @WebMethod
    @Override
    @Nullable
    public String getUserIdByNumber(@Nullable final Session session, final int number) throws Exception {
        validateSession(session);
        return userService.getIdByNumber(number);
    }

    @WebMethod
    @Override
    @Nullable
    public User getUserByLoginE(@Nullable final Session session, @Nullable final String login) throws Exception {
        validateSession(session);
        return userService.getUserByLogin(login);
    }

    @WebMethod
    @Override
    public void loadDataBin(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.loadDataBin();
    }

    @WebMethod
    @Override
    public void loadDataJsonByFasterXml(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.loadDataJsonByFasterXml();
    }

    @WebMethod
    @Override
    public void loadDataXmlByFasterXml(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.loadDataXmlByFasterXml();
    }

    @WebMethod
    @Override
    public void loadDataJsonByJaxB(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.loadDataJsonByJaxB();
    }

    @WebMethod
    @Override
    public void loadDataXmlByJaxB(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.loadDataXmlByJaxB();
    }

    @WebMethod
    @Override
    public void saveDataBin(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.saveDataBin(session.getUserId());
    }

    @WebMethod
    @Override
    public void saveDataJsonByFasterXml(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.saveDataJsonByFasterXml(session.getUserId());
    }

    @WebMethod
    @Override
    public void saveDataXmlByFasterXml(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.saveDataXmlByFasterXml(session.getUserId());
    }

    @WebMethod
    @Override
    public void saveDataJsonByJaxB(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.saveDataJsonByJaxB(session.getUserId());
    }

    @WebMethod
    @Override
    public void saveDataXmlByJaxB(@Nullable final Session session) throws Exception {
        validateSession(session);
        userService.saveDataXmlByJaxB(session.getUserId());
    }
}

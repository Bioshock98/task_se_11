package ru.pyshinskiy.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull private ITaskService taskService;

    public TaskEndpoint() {
        super();
    }

    public TaskEndpoint(@NotNull final ISessionService sessionService, @NotNull final ITaskService taskService) {
        super(sessionService);
        this.taskService = taskService;
    }

    @WebMethod
    @Nullable
    @Override
    public Task findOneTask(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return taskService.findOne(id);
    }

    @WebMethod
    @NotNull
    @Override
    public List<Task> findAllTasks(@Nullable final Session session) throws Exception {
        validateSession(session);
        return taskService.findAll();
    }

    @WebMethod
    @Nullable
    @Override
    public Task persistTask(@Nullable final Session session, @Nullable final Task t) throws Exception {
        validateSession(session);
        return taskService.persist(t);
    }

    @WebMethod
    @Override
    @Nullable
    public List<Task> persistTasks(@Nullable final Session session, @Nullable final List<Task> ts) throws Exception {
        validateSession(session);
        return taskService.persist(ts);
    }

    @WebMethod
    @Nullable
    @Override
    public Task mergeTask(@Nullable final Session session, @Nullable final Task t) throws Exception {
        validateSession(session);
        return taskService.merge(t);
    }

    @WebMethod
    @Nullable
    @Override
    public Task removeTask(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return taskService.remove(id);
    }

    @WebMethod
    @Override
    public void removeAllTasks(@Nullable final Session session) throws Exception {
        validateSession(session);
        taskService.removeAll();
    }

    @WebMethod
    @Override
    @Nullable
    public String getTaskIdByNumber(@Nullable final Session session, final int number) throws Exception {
        validateSession(session);
        return taskService.getIdByNumber(number);
    }

    @WebMethod
    @Override
    @Nullable
    public Task findOneTaskByUserId(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return taskService.findOne(session.getUserId(), id);
    }

    @WebMethod
    @Override
    @NotNull
    public List<Task> findAllTasksByUserId(@Nullable final Session session) throws Exception{
        validateSession(session);
        return taskService.findAll(session.getUserId());
    }

    @WebMethod
    @Override
    @Nullable
    public Task removeTaskByUserId(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return taskService.remove(id);
    }

    @WebMethod
    @Override
    public void removeAllTasksByUserId(@Nullable final Session session) throws Exception {
        validateSession(session);
        taskService.remove(session.getUserId());
    }

    @WebMethod
    @Override
    @Nullable
    public List<Task> findTasksByName(@Nullable final Session session, @Nullable final String name) throws Exception {
        validateSession(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    @Nullable
    public List<Task> findTasksByDescription(@Nullable final Session session, @Nullable final String description) throws Exception {
        validateSession(session);
        return taskService.findByDescription(session.getUserId(), description);
    }

    @WebMethod
    @NotNull
    public List<Task> sortTasksByCreateTime(@Nullable final Session session, @NotNull final List<Task> tasks, final int direction) throws Exception {
        validateSession(session);
        return taskService.sortByCreateTime(tasks, direction);
    }

    @WebMethod
    @NotNull
    public List<Task> sortTasksByStartDate(@Nullable final Session session, @NotNull final List<Task> tasks, final int direction) throws Exception {
        validateSession(session);
        return taskService.sortByStartDate(tasks, direction);
    }

    @WebMethod
    @NotNull
    public List<Task> sortTasksByFinishDate(@Nullable final Session session, @NotNull final List<Task> tasks, final int direction) throws Exception {
        validateSession(session);
        return taskService.sortByFinishDate(tasks, direction);
    }

    @WebMethod
    @NotNull
    public List<Task> sortTasksByStatus(@Nullable final Session session, @NotNull final List<Task> tasks, final int direction) throws Exception {
        validateSession(session);
        return taskService.sortByStatus(tasks, direction);
    }

    @WebMethod
    @NotNull
    @Override
    public List<Task> findTasksByProjectId(@Nullable final Session session, @Nullable final String projectId) throws Exception {
        validateSession(session);
        return taskService.findAllByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    @Override
    public void removeTasksByProjectId(@Nullable final Session session, @Nullable final String projectId) throws Exception {
        validateSession(session);
        taskService.removeAllByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    @Override
    public void removeAllAttachedTasks(@Nullable final Session session) throws Exception {
        validateSession(session);
        taskService.removeAttachedTasks();
    }

    @WebMethod
    @Override
    public void removeAllAttachedTasksByUserId(@Nullable final Session session) throws Exception {
        validateSession(session);
        taskService.removeAttachedTasks(session.getUserId());
    }
}

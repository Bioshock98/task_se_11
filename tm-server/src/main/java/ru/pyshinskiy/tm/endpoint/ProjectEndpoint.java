package ru.pyshinskiy.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull private IProjectService projectService;

    public ProjectEndpoint() {
        super();
    }

    public ProjectEndpoint(@NotNull final ISessionService sessionService, @NotNull final IProjectService projectService) {
        super(sessionService);
        this.projectService = projectService;
    }

    @WebMethod
    @Nullable
    @Override
    public Project findOneProject(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return projectService.findOne(id);
    }

    @WebMethod
    @NotNull
    @Override
    public List<Project> findAllProjects(@Nullable final Session session) throws Exception {
        validateSession(session);
        return projectService.findAll();
    }

    @WebMethod
    @Nullable
    @Override
    public Project persistProject(@Nullable final Session session, @Nullable final Project t) throws Exception {
        validateSession(session);
        return projectService.persist(t);
    }

    @NotNull
    @WebMethod
    @Override
    public List<Project> persistProjects(@Nullable final Session session, @Nullable final List<Project> ts) throws Exception {
        validateSession(session);
        return projectService.persist(ts);
    }

    @WebMethod
    @Nullable
    @Override
    public Project mergeProject(@Nullable final Session session, @Nullable final Project t) throws Exception {
        validateSession(session);
        return projectService.merge(t);
    }

    @WebMethod
    @Override
    @Nullable
    public Project removeProject(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return projectService.remove(id);
    }

    @WebMethod
    @Override
    public void removeAllProjects(@Nullable final Session session) throws Exception {
        validateSession(session);
        projectService.removeAll();
    }

    @WebMethod
    @Override
    @Nullable
    public String getProjectIdByNumber(@Nullable final Session session, final int number) throws Exception {
        validateSession(session);
        return projectService.getIdByNumber(number);
    }

    @WebMethod
    @Override
    @Nullable
    public Project findOneProjectByUserId(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return projectService.findOne(session.getUserId(), id);
    }

    @WebMethod
    @Override
    @NotNull
    public List<Project> findAllProjectsByUserId(@Nullable final Session session) throws Exception{
        validateSession(session);
        return projectService.findAll(session.getUserId());
    }

    @WebMethod
    @Override
    @Nullable
    public Project removeProjectByUserId(@Nullable final Session session, @Nullable final String id) throws Exception {
        validateSession(session);
        return projectService.remove(id);
    }

    @WebMethod
    @Override
    public void removeAllProjectsByUserId(@Nullable final Session session) throws Exception {
        validateSession(session);
        projectService.remove(session.getUserId());
    }

    @WebMethod
    @Override
    @Nullable
    public List<Project> findProjectByName(@Nullable final Session session, @Nullable final String name) throws Exception {
        validateSession(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    @Nullable
    public List<Project> findProjectByDescription(@Nullable final Session session, @Nullable final String description) throws Exception {
        validateSession(session);
        return projectService.findByDescription(session.getUserId(), description);
    }

    @WebMethod
    @NotNull
    public List<Project> sortProjectsByCreateTime(@Nullable final Session session, @NotNull final List<Project> tasks, final int direction) throws Exception {
        validateSession(session);
        return projectService.sortByCreateTime(tasks, direction);
    }

    @WebMethod
    @NotNull
    public List<Project> sortProjectsByStartDate(@Nullable final Session session, @NotNull final List<Project> tasks, final int direction) throws Exception {
        validateSession(session);
        return projectService.sortByStartDate(tasks, direction);
    }

    @WebMethod
    @NotNull
    public List<Project> sortProjectsByFinishDate(@Nullable final Session session, @NotNull final List<Project> tasks, final int direction) throws Exception {
        validateSession(session);
        return projectService.sortByFinishDate(tasks, direction);
    }

    @WebMethod
    @NotNull
    public List<Project> sortProjectsByStatus(@Nullable final Session session, @NotNull final List<Project> tasks, final int direction) throws Exception {
        validateSession(session);
        return projectService.sortByStatus(tasks, direction);
    }
}
